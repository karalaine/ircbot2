#ifndef BOT_HPP
#define BOT_HPP
#include <cctype>
#include <string>
#include <fstream>
#include <memory>

#include "irc.hpp"

class Bot
{
public:
	Bot(const std::string &nick,const std::string &address,const std::string &port);
	virtual ~Bot();
	virtual void Update();
	virtual void Say(const std::string &channel,const std::string &message);
	void JoinChannel(const std::string &channel);
	void SetQuitFlag(bool& quit){this->quit = &quit; }

protected:
	std::unique_ptr<Irc> server;
	bool *quit;
	std::string buffer;
private:
	std::string channel;
	std::string findTitle(const std::string &html);
	void checkForDb(const std::string &url, const std::string &nick);
	void findLink(const std::string &msg, const std::string &nick);
	void toLowerCase(std::string &str);


};

#endif
