#include <iostream>
#include <soci/soci.h>
#include <soci/sqlite3/soci-sqlite3.h>
#include <boost/algorithm/string/trim.hpp>
#include <ctime>
#include <exception>
#include "bot.hpp"
#include "CurlObject.h"
#include "entities.h"

Bot::Bot(const std::string &nick,const std::string &address,const std::string &port)
    : quit(nullptr)
{
	server = std::unique_ptr<Irc> (new Irc(nick));

	server->Connect(address,port);
}
void Bot::JoinChannel(const std::string &channel){
	boost::system::error_code error;
	this->channel = channel;
	toLowerCase(this->channel);
	server->Send("JOIN " + this->channel + "\r\n", error);
}

void Bot::Update()
{
	boost::system::error_code error;
	buffer = server->Receive(error);
	if(error == boost::asio::error::eof || !server->isConnected())
	{
		if(quit != nullptr)
			{
			 	*quit = true;
			}
	}
	else
	{
		if(buffer.find("PRIVMSG") != std::string::npos)
		{
			std::string msgnick, msgchannel, type, message;
			Irc::ParseMessage(buffer, msgnick, msgchannel, type, message);
			toLowerCase(msgchannel);
			std::cout << "[" << channel << "] <" << msgnick << "> " << message;
			if (channel == msgchannel || msgnick == "karalaine")
			{
				findLink(message,msgnick);
			}
			if(message.find("JOIN #") != std::string::npos)
			{
				server->Send(message,error);
			}
			else if(message == "QUIT\r\n" && msgnick == "karalaine")
			{
				server->Send(message, error);
				server->Disconnect();

			}

			else if(message.find("COMMAND") != std::string::npos && msgnick == "karalaine")
			{
			  server->Send(message.erase(0,8),error); //erase "command" from the begin
			}
		}
		else
		{
			std::cout << buffer;
		}
	}
}
void Bot::Say(const std::string &channel,const std::string &message)
{
	boost::system::error_code error;
	server->Send("PRIVMSG " + channel + " :" + message +"\r\n",error);
}
void Bot::toLowerCase( std::string &str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

std::string Bot::findTitle(const std::string &htmlData)
{
	std::string lowData = htmlData;
	toLowerCase(lowData);
    size_t pos = lowData.find("</title");
	if(pos != std::string::npos)
    {
        std::string header = htmlData.substr(0,pos);
        pos = header.rfind('>');
        if(pos != std::string::npos)
        {
            return header.substr(pos+1,header.length());
        }
    }
    return "";
}


void Bot::checkForDb(const std::string &url, const std::string &nick)
{
    try
    {
        soci::session sql(soci::sqlite3, "urldb");
        soci::row r;
        sql << "select nick, date from entries where url = :url", soci::into(r), soci::use(url);
        if(sql.got_data())
        {
            std::string sender;
            std::tm date;
            r >> sender >> date;
            date.tm_hour += 2;
            if(date.tm_hour >= 24)
            {
                date.tm_hour -= 24;
            }

            std::string mesg = "Wanha! " + sender + " lähetti tämän "  + asctime(&date);
            std::cout << mesg;
            Say(channel, mesg);
        }
        else
        {
            sql << "insert into entries values(:url, :nick, datetime('now'))", soci::use(url),soci::use(nick);
        }
  }
  catch (std::exception const &e)
   {
     std::cerr << "Error in database reading: " << e.what() << '\n';
   }
}



void Bot::findLink(const std::string &msg, const std::string &nick)
{
    std::string lowMsg = msg;
    std::string upMsg = msg;
    toLowerCase(lowMsg);
    size_t pos = lowMsg.find("http");
    if(pos == std::string::npos)
    {
        pos = lowMsg.find("www.");
        if(pos == std::string::npos)
        {
            pos = lowMsg.find("ftp");
            if(pos == std::string::npos)
            {
                return;
            }
        }

    }
    else
    {
	std::string spotStr = msg;
	size_t spotify = spotStr.find("play.spotify");
	if(spotify != std::string::npos)
	{
            std::cout << "Found bad spotify URL";
	    std::string correctURL = spotStr.replace(spotify,4,"open");
	    Say(channel, nick + " pls, use open.spotify.com like this: " + correctURL);
	    upMsg = spotStr;
	}
    }
    size_t endPos = upMsg.substr(pos,upMsg.length()).find(" ");

    std::string foundLink = upMsg.substr(pos,endPos);
	boost::algorithm::trim(foundLink);
    std::cout << "Found link: " << foundLink;
    CurlObject curl(foundLink);
    std::string htmlPage = curl.getData();
    if(!htmlPage.empty())
    {
      //std::cout << "file contents: " << contents;
        std::string foundTitle = findTitle(htmlPage);
		boost::algorithm::trim(foundTitle);
        if(!foundTitle.empty())
        {
            char * cstr = new char[foundTitle.size()+1];
            decode_html_entities_utf8(cstr,foundTitle.c_str());
            foundTitle = cstr;
            Say(channel, "URL: " + foundTitle);
            delete[] cstr;
        }
        else
        {
            std::cout << "No title found for html";
        }
        checkForDb(foundLink, nick);
    }
    else
    {
        std::cout << "failed to fetch url";
    }

}

Bot::~Bot()
{
}
