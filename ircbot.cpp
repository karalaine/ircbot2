#include "bot.hpp"
#ifndef WIN32
#include "daemon.hpp"
#endif

int main(int argc, char* argv[])
{
	for(;;)
    {
		#ifndef WIN32
		daemonize(".", "in.log", "out.log", "err.log");
		#endif
		std::string nick= "JormaEvo2", server = "irc.quakenet.org";
		if(argc == 3)
		{
			nick = argv[1];
			server= argv[2];
		}
		try
		{
			Bot bot(nick, server, "6660");
			bot.JoinChannel("#kctite09");
			bool quit_flag = false;
			bot.SetQuitFlag(quit_flag);
			while(!quit_flag)
			{
				bot.Update();
			}
		}
		catch(std::exception const &e)
		{
			std::cout << "Unhandled exception" << e.what() << std::endl;
		}
	}
	return 0;
}
