CXXFLAGS = -Wall -fmessage-length=0 -std=c++0x -I~/src/soci/soci/src/core

OBJS =	ircbot.o bot.o entities.o irc.o CurlObject.o

LIBS = -lboost_date_time -lboost_system -lcurl -lpthread -lsoci_core -lsoci_sqlite3

TARGET = IrcBot2

Debug:	CXXFLAGS += -DDEBUG -g

$(TARGET):  $(OBJS)
	$(CXX) -o bin/Release/$(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)


clean:
	rm -f $(OBJS) bin/Debug/$(TARGET) bin/Release/$(TARGET)
