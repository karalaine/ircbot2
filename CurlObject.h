#ifndef CURLOBJECT_H
#define CURLOBJECT_H
#include <string>
#include <curl/curl.h>
class CurlObject
{
    public:
        CurlObject(const std::string&);
        virtual ~CurlObject();
        std::string getData();
    protected:
    private:
        CURL *curl;
        std::string buffer;
        size_t received_DataImpl(void *ptr, size_t size, size_t nmemb);
        static size_t writeHandle(void *ptr, size_t size, size_t nmemb, void * p);
};

#endif // CURLOBJECT_H
