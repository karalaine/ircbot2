#include "CurlObject.h"

CurlObject::CurlObject(const std::string &url)
{

	curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &CurlObject::writeHandle); //callback function for data received
//	curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, received_Header);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION,1);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER,0 );
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0");
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
}

CurlObject::~CurlObject()
{
    curl_easy_cleanup(curl);
}

size_t CurlObject::writeHandle(void * data, size_t size, size_t nmemb, void * p)
{
    return static_cast<CurlObject*>(p)->received_DataImpl(data, size, nmemb);
}

size_t CurlObject::received_DataImpl(void *ptr, size_t size, size_t nmemb)
{
  buffer.append((char*)ptr, size*nmemb);
      return size*nmemb;
}
size_t received_Header( char *ptr, size_t size, size_t nmemb)
{
	std::string data(ptr, size*nmemb);
	 std::size_t found = data.find("html");
	if(found != std::string::npos)
		return size*nmemb;
	else
		return -1;
}
std::string CurlObject::getData()
{
  //  m_pBuffer = (char*) malloc(20000 * sizeof(char));
    CURLcode res;
	if(curl)
	{
		/* Perform the request, res will get the return code */
		buffer.clear(); //make sure that buffer is empty
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK)
		  {
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				  curl_easy_strerror(res));
		  }
    }
    return buffer;
}
