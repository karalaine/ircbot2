#include <exception>
#include <sstream>
#include <iostream>
#include <memory>
#include "irc.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace boost::asio;
Irc::Irc(const std::string nick_)
{
	nick = nick_;
	typedef ip::tcp::socket socket;
	server = std::unique_ptr<socket> (new socket(io_serv));
	connected = false;
}
Irc::~Irc()
{
    
}
void Irc::Connect(const std::string address,const std::string port)
{
	ip::tcp::resolver resolver(io_serv);
	ip::tcp::resolver::query query(address,port);
	server->connect(*(resolver.resolve(query)));
	std::string pribuffer;
    size_t pos=std::string::npos;
	while(true){
		boost::asio::deadline_timer timer(io_serv,boost::posix_time::milliseconds(15));
		timer.wait();
		 boost::system::error_code error;
		std::string temp = Receive(error);
		std::cout << temp;
		pribuffer += temp;
		if (error == boost::asio::error::eof)
			break; // Connection closed cleanly by peer.
		if(pribuffer.find("NOTICE AUTH :*** No ident response") != std::string::npos){
			std::string msg = "NICK " + nick + "\r\n";
			Send(msg, error);
			msg = "USER foo a a :foo bar\r\n";
			Send(msg, error);
			pribuffer.clear();
		}
		else if(pribuffer.find("Nickname is already in use.")!= std::string::npos){
			nick += '-';
			Send("NICK " + nick + "\r\n", error);
			std::string msg = "USER jor a a :jor moi\r\n";
			Send(msg,error);
			pribuffer.clear();
		}
		else if((pos=pribuffer.find('\n',pribuffer.find("PING :"))) != std::string::npos){
			Pong(pribuffer,error);
			return;
		}
		if (error == boost::asio::error::eof)
			break; // Connection closed cleanly by peer.
	}
}

void Irc::Disconnect()
{
	server->close();
}



void Irc::Send(const std::string &message, boost::system::error_code error)
{
	if(server->write_some(boost::asio::buffer(message.c_str(),message.length()),error) < message.size()){
		Disconnect();
	}
	std::cout << message;
}
void Irc::Pong(const std::string &message,boost::system::error_code error)
{
    std::string ping = message;
    size_t pos = ping.find("PING");
	if(pos == std::string::npos)
        return;
	ping[pos+1] = 'O';
	ping += "\r\n";
	Send(ping,error);
}
std::string Irc::Receive(boost::system::error_code error)
{
	std::string temp;
	if(!bufferi.empty()){
		if(bufferi.begin()->find('\n') != std::string::npos){
			temp = (*bufferi.begin());
			bufferi.erase(bufferi.begin());
			return temp;
		}
	}
	memset(buffer,'0',512); //clear the buffer
	int received = server->read_some(boost::asio::buffer(buffer,512), error);
	if(received <= 0){ //found error
		connected = false;
        Disconnect();
		return "";
	}
	connected = true;
	buffer[received] = 0;
	temp = buffer;
	for(bufferi_iter = bufferi.begin(); bufferi_iter != bufferi.end(); ++bufferi_iter){
		if(bufferi_iter->find('\n') == std::string::npos){ //partial line in buffer
			(*bufferi_iter) += temp.substr(0,temp.find('\n')+1); //insert rest of the line
			temp.erase(0,temp.find('\n')+1); //erase inserted line from temp
		}
	}
	while(temp.find('\n') != std::string::npos){ //insert lines to bufferi
		bufferi.push_back(temp.substr(0,temp.find('\n')+1));
		temp.erase(0,temp.find('\n')+1); //erase inserted line from temp
	}
	if(bufferi.empty()){
		bufferi.push_back(temp);
		return "";
	}
	temp = (*bufferi.begin());
    size_t pos=std::string::npos;
	if((pos=temp.find("PING :")) != std::string::npos)
		Pong(buffer,error);
	bufferi.erase(bufferi.begin());
	return temp;
}
void Irc::ParseMessage(std::string message, std::string &nick, std::string &channel, std::string &type, std::string &msg)
{
	message.erase(0,1);
	size_t pos = message.find('!');
	if(pos == std::string::npos)
		return;
	nick = message.substr(0,pos);
	message.erase(0,nick.length());
	pos = message.find(' ');
	if(pos == std::string::npos)
		return;
	message.erase(0,pos+1);
	pos = message.find(' ');
	if(pos == std::string::npos)
		return;
	type = message.substr(0,pos);
	message.erase(0,type.length()+1);
	pos = message.find(':');
	if(pos == std::string::npos)
		return;
	channel = message.substr(0,pos-1);
	message.erase(0,channel.length()+2);
	msg = message;
}
