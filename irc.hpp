#ifndef IRC_HPP
#define IRC_HPP
#include <string>
//#define _WIN32_WINNT 0x0501
#include <boost/asio.hpp>
#include <vector>
#include <memory>

class Irc
{
public:
	Irc(const std::string nick_);
    ~Irc();
	void Connect(const std::string address, const std::string port);
	void Disconnect();
	void Send(const std::string &message, boost::system::error_code error);
	std::string Receive(boost::system::error_code error); //returns one \r\n terminated line at a time
	static void ParseMessage(std::string message,std::string &nick,std::string &channel,std::string &type,std::string &msg);
	std::string GetNick() const { return nick; }
	bool isConnected() const {return connected;}
	void setNick(const std::string& nick);
private:
	Irc& operator=(const Irc &irc);
    Irc(const Irc &irc);
	void Pong(const std::string &message,boost::system::error_code error); //responses to ping
	std::vector<std::string> bufferi;
	std::vector<std::string>::iterator bufferi_iter;
	std::string nick;
	char buffer[512];
  boost::asio::io_service io_serv;
	std::unique_ptr<boost::asio::ip::tcp::socket> server;
	bool connected;

};

#endif
